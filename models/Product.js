const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Please input the name of the product"]
	},

	description: {
		type: String,
		required: [true, "Please input a basic description of the product"]
	},

	price: {
		type: Number,
		required: [true, "Please input the price of the product"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	orderArchive: [
		{
			
			userId: {
				type: String,
				required: [true, "Please input the ID of the user"]
			},

			totalAmount: {
				type: Number,
				required: [true, "Please input the amount ordered"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}

		}]


})

module.exports = mongoose.model("Product", productSchema)