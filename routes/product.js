const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");


router.post("/", auth.verify, (req, res) => {
	const productData = auth.decode(req.headers.authorization)

	if (productData.isAdmin === true) {

		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You are not authorized for this action.`)
	}


})


router.get("/products", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));

})


router.get("/products/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})


router.put("/products/:productId", auth.verify, (req, res) => {

	const productData = auth.decode(req.headers.authorization)

	if (productData.isAdmin === true) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You are not authorized for this action.`)
	}
})


router.put("/products/:productId/archive", auth.verify, (req, res) => {

	const productData = auth.decode(req.headers.authorization)

	if (productData.isAdmin === true) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You are not authorized for this action.`)
	}
})


module.exports = router;