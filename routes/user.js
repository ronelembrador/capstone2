const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth")


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))

})


// Login for user to get Bearer token for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// To change a user's privilege as Admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: req.params.userId,
		isAdmin: userData.isAdmin
	}

	if (userData.isAdmin === false) {
			userController.setAdmin(data).then(resultFromController => res.send(resultFromController))
		} else {
			res.send(`The user is already an Admin`)
		}
	
})


router.post("/checkout", auth.verify, (req, res) => {

	const isUserLoggedIn = auth.decode(req.headers.authorization)

	if (isUserLoggedIn.isAdmin === false) {
	let data = {
		userId: isUserLoggedIn.id,
		productId: req.body.productId,
		totalAmount: req.body.totalAmount
	}

	userController.orderCheckout(data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(`You are not authorized for this action`)
	}
})


router.get("/orders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {

		userController.getOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send(`You are not authorized for this action`)	
	}
})


router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === false) {

		let data = userData

		userController.getUsersOrders(data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(`You are not authorized for this action`)	
	}

})

module.exports = router;